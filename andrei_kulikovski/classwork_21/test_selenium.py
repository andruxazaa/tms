import pytest
from selenium.webdriver.common.by import By


MAIN_PAGE_URL = "https://the-internet.herokuapp.com/login"


def test_selenium_positive(web, link):
    driver = web
    element1 = driver.find_element(By.ID, "username")
    element1.send_keys("tomsmith")
    element2 = driver.find_element(By.ID, "password")
    element2.send_keys("SuperSecretPassword!")
    driver.find_element(By.CSS_SELECTOR, 'button[type="submit"]').click()
    login = driver.find_element(By.ID, "flash")
    assert login.text == "You logged into a secure area!\n×"


def test_selenium_negative(web, link):
    driver = web
    element1 = driver.find_element(By.ID, "username")
    element1.send_keys("toms")
    element2 = driver.find_element(By.ID, "password")
    element2.send_keys("SuperSecretPassword")
    driver.find_element(By.CSS_SELECTOR, 'button[type="submit"]').click()
    login = driver.find_element(By.ID, "flash")
    assert login.text == "Your username is invalid!\n×"


@pytest.mark.xfail
def test_selenium_fail(web, link):
    driver = web
    element1 = driver.find_element(By.ID, "username")
    element1.send_keys("toms")
    element2 = driver.find_element(By.ID, "password")
    element2.send_keys("SuperSecretPassword")
    driver.find_element(By.CSS_SELECTOR, 'button[type="submit"]').click()
    login = driver.find_element(By.ID, "flash")
    assert login.text == "You logged into a secure area!\n×"
