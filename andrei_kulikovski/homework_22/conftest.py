import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from andrei_kulikovski.homework_22.test_locators import URL, USER_NAME,\
    PASSWORD, LOGIN


@pytest.fixture()
def driver():
    driver = webdriver.Chrome(executable_path="D:/QA python/Python/"
                                              "chromedriver")
    yield driver
    driver.quit()


@pytest.fixture()
def link(driver):
    return driver.get(URL)


@pytest.fixture()
def login(driver):
    driver.find_element(By.CSS_SELECTOR, USER_NAME).send_keys("standard_user")
    driver.find_element(By.CSS_SELECTOR, PASSWORD).send_keys("secret_sauce")
    driver.find_element(By.CSS_SELECTOR, LOGIN).click()
