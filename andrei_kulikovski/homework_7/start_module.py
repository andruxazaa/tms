import random


"""
Общий модуль
Создать модуль, который будет предоставлять возможность выбрать одну из двух
предыдущих программ и запустить ее, также он должен позволить по окончанию
выполнения программы выбрать запуск другой!
Если пользователь совершит ошибку при выборе программы
Первые три раза программа должна сообщить ему об ошибке, а на четвертый раз
запустить одну из программ, выбранных случайно
"""


def module():
    """the function launches the selected module,
    there are 4 attempts to launch otherwise a random module is launched,
    next restart or exit.
    """
    count = 0
    while count < 4:
        count += 1
        var_mod = int(input("1 - Крестики нолики, 2 - Шифр цезаря,"
                            " 3 - Банковский вклад, 4 - выход: "))
        if count >= 4:
            var_mod = random.randint(1, 3)
        if var_mod == 1:
            import task1.task1
            print(task1.task1)
        elif var_mod == 2:
            import task2.task2
            print(task2.task2)
        elif var_mod == 3:
            import task3.task3
            print(task3.task3)
        elif var_mod == 4:
            exit()
        else:
            print("ошибка!")
    return


module()
while True:
    module()
