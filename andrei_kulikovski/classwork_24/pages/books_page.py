from pages.base_page import BasePage
from pages.books_locators import BooksPageLocators


class BooksPage(BasePage):
    URL = "http://selenium1py.pythonanywhere.com/en-gb"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def open_books_page(self):
        books = self.driver.find_element(*BooksPageLocators.BOOKS_LINK)
        books.click()

    def open_book(self):
        book = self.driver.find_element(*BooksPageLocators.BOOK_LINK)
        book.click()

    def img(self):
        image = self.driver.find_element(*BooksPageLocators.BOOK_IMG)
        return image

    def name(self):
        name = self.driver.find_element(*BooksPageLocators.BOOK_NAME)
        return name

    def price(self):
        price = self.driver.find_element(*BooksPageLocators.BOOK_PRICE)
        return price
