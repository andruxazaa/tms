from selenium.webdriver.common.by import By


class BooksPageLocators:
    BOOKS_LINK = (By.CSS_SELECTOR, '.dropdown-submenu')
    BOOK_LINK = (By.XPATH, '//*[@title="Coders at Work"]')
    BOOK_IMG = (By.XPATH, '//img')
    BOOK_NAME = (By.XPATH, '//*[@class="col-sm-6 product_main"]/h1')
    BOOK_PRICE = (By.XPATH, '//*[@class="price_color"]')
