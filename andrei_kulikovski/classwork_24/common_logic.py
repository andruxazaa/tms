from selenium.webdriver.common.by import By


def test_main_page_old(driver):
    URL = "http://selenium1py.pythonanywhere.com/en-gb"
    driver.get(URL)
    login_link = driver.find_element(By.CSS_SELECTOR, "#login_link")
    login_link.click()
    assert "/accounts/login/" in driver.current_url,\
        "Incorrect page has been opened"
