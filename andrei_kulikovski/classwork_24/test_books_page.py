from pages.books_page import BooksPage


def test_open_books(driver):
    page = BooksPage(driver)
    page.open_page()
    page.open_books_page()
    assert driver.title == "Books | Oscar - Sandbox"
    page.open_book()
    assert page.img().is_displayed
    assert page.name().text == "Coders at Work"
    assert page.price().text == "£19.99"
    assert driver.title == "Coders at Work | Oscar - Sandbox"
