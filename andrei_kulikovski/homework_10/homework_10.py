from statistics import mean
"""
Цветочница.

Определить иерархию и создать несколько цветов (Rose, Tulips, violet,
chamomile). У каждого класса цветка следующие атрибуты:
Стоимость – атрибут класса, который определяется заранее
Свежесть (в днях), цвет, длинна стебля – атрибуты экземпляра
При попытке вывести информацию о цветке, должен отображаться цвет и тип
Rose = RoseClass(7, Red, 15)
Print(Rose) -> Red Rose (__str__)
Собрать букет (можно использовать аксессуары – отдельные классы со своей
стоимостью: например, упаковочная бумага) с определением ее стоимости.
У букета должна быть возможность определить время его увядания по среднему
времени жизни всех цветов в букете
Позволить сортировку цветов в букете на основе различных параметров
(свежесть/цвет/длина стебля/стоимость...)
Реализовать поиск цветов в букете по определенным параметрам.
Узнать, есть ли цветок в букете. (__contains__)
Добавить возможность получения цветка по индексу (__getitem__) либо возможность
пройтись по букету и получить каждый цветок по-отдельности (__iter__)
"""


class Flowers:
    def __init__(self, day, color, long):
        self.day = day
        self.color = color
        self.long = long


class Rose(Flowers):
    cash = 10

    def __str__(self):
        return f"{self.color} {Rose.__name__}"


class Tulips(Flowers):
    cash = 8

    def __str__(self):
        return f"{self.color} {Tulips.__name__}"


class Violet(Flowers):
    cash = 5

    def __str__(self):
        return f"{self.color} {Violet.__name__}"


class Chamomile(Flowers):
    cash = 3

    def __str__(self):
        return f"{self.color} {Chamomile.__name__}"


class Bouquet:
    papier = 2

    def __init__(self, flowers):
        self.flowers = flowers

    def __getitem__(self, index):
        return self.flowers[index]

    def __contains__(self, obj):
        return obj in self.flowers

    def __iter__(self):
        return iter(self.flowers)

    @property
    def bouquet_price(self):
        bouquet_sum = sum(arg.cash for arg in self.flowers)
        return bouquet_sum + Bouquet.papier

    @property
    def time_life(self):
        time_life = mean(arg.day for arg in self.flowers)
        return time_life

    @property
    def sort_day(self):
        return sorted(self.flowers, key=lambda flow: flow.day)

    @property
    def sort_color(self):
        return sorted(self.flowers, key=lambda flow: flow.color)

    @property
    def sort_long(self):
        return sorted(self.flowers, key=lambda flow: flow.long)

    @property
    def sort_cash(self):
        return sorted(self.flowers, key=lambda flow: flow.cash)

    def search_day(self, arg):
        res = None
        for x in bouquet:
            if x.day == arg:
                res = x
                break
        print("Поиск", res)

    def search_color(self, arg):
        res = None
        for x in bouquet:
            if x.color == arg:
                res = x
                break
        print("Поиск", res)

    def search_long(self, arg):
        res = None
        for x in bouquet:
            if x.long == arg:
                res = x
                break
        print("Поиск", res)


rose = Rose(7, "Red", 15)
tulips = Tulips(4, "Yellow", 10)
violet = Violet(6, "Purple", 11)
chamomile = Chamomile(5, "White", 8)
bouquet = Bouquet([rose, tulips, violet, chamomile])


print(rose, tulips, violet, chamomile)
print("Букет цена", bouquet.bouquet_price)
print("Увядание", bouquet.time_life)
print("Свежесть", bouquet.sort_day)
print("Цвет", bouquet.sort_color)
print("Стебель", bouquet.sort_long)
print("Цена", bouquet.sort_cash)
bouquet.search_day(6)
bouquet.search_color('Yellow')
bouquet.search_long(8)
print(violet in bouquet)
print(bouquet[0])
for f in bouquet:
    print(f)
