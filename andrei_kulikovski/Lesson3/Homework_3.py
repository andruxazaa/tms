import random
import math

"""
1. Заменить символ “#” на символ “/” в строке 'www.my_site.com#about'
"""
msg = 'www.my_site.com#about'
print(msg.replace("#", "/"))


"""
2. В строке “Ivanou Ivan” поменяйте местами слова => "Ivan Ivanou"
"""
name = "Ivan"
s_name = "Ivanou"
print(f"{s_name} {name}")


"""
3. Напишите программу которая удаляет пробел в начале строки
"""
string = input()
print(string.lstrip())


"""
4. Напишите программу которая удаляет пробел в конце строки
"""
string = input()
print(string.rstrip())


"""
5. a = 10, b=23, поменять значения местами,
чтобы в переменную “a” было записано значение “23”, в “b” - значение “-10”
"""
a = 10
b = 23
a, b = b, -a
print(a, b)


"""
6. значение переменной “a” увеличить в 3 раза, а значение “b” уменьшить на 3
"""
a = a * 3
b = b - 3
print(a, b)


"""
7. преобразовать значение “a” из целочисленного в число
с плавающей точкой (float), а значение в переменной “b” в строку
"""
print(float(a))
print(str(b))


"""
8. Разделить значение в переменной “a” на 11 и вывести результат
с точностью 3 знака после запятой
"""
print(round(a / 11, 3))


"""
9. Преобразовать значение переменной “b” в число с плавающей точкой и
записать в переменную “c”. Возвести полученное число в 3-ю степень.
"""
c = float(b)
print(pow(c, 3))
"""
10. Получить случайное число, кратное 3-м
"""
x = random.randrange(0, 100, 3)
print(x)


"""
11. Получить квадратный корень из 100 и возвести в 4 степень
"""
y = pow((math.sqrt(100)), 4)
print(y)


"""
12. Строку “Hi guys” вывести 3 раза и в конце добавить “Today”
“Hi guysHi guysHi guysToday”
"""
s1 = "Hi guys"
s2 = "Today"
s3 = s1 * 3 + s2
print(s3)

"""
13. Получить длину строки из предыдущего задания
"""
print(len(s3))
"""
14. Взять предыдущую строку и вывести слово “Today”
в прямом и обратном порядке
"""
word = s3[-5:]
print(word)
print(word[::-1])


"""
15. “Hi guysHi guysHi guysToday” вывести каждую 2-ю букву
в прямом и обратном порядке
"""
c_word = "Hi guysHi guysHi guysToday"
print(c_word.replace(' ', '')[1::2])
print(c_word.replace(' ', '')[-2::-2])


"""
16. Используя форматирования подставить результаты из задания 10 и 11
в следующую строку “Task 10: <в прямом>, <в обратном> Task 11: <в прямом>,
<в обратном>”
"""
x1 = str(x)
y1 = str(y)
print(f"Task 10: {x1}, {x1[::-1]} Task 11: {y1}, {y1[::-1]}")


"""
17. Есть строка: “my name is name”. Напечатайте ее,
но вместо 2ого “name” вставьте ваше имя.
"""
# variants
msg = "my name is name"
print(msg.replace("is name", "is Andrei"))

name = "Andrei"
print(f"my name is {name}")

msg = "my name is name"
list_msg = msg.split(" ")
list_msg[3] = "Andrei"
new_msg = " ".join(list_msg)
print(new_msg)


"""
18. Полученную строку в задании 12 вывести:
а) Каждое слово с большой буквы
б) все слова в нижнем регистре
в) все слова в верхнем регистре
"""
print("Title\t\t", s3.title())
print("Lower\t\t", s3.lower())
print("Upper\t\t", s3.upper())


"""
19. Посчитать сколько раз слово “Task” встречается в строке из задания 12
"""
print(s3.count("Task"))
