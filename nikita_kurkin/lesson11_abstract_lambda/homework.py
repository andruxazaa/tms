from abc import ABC, abstractmethod

"""
ФЕРМА
На ферме могут жить животные и птицы
И у тех и у других есть два атрибута: имя и возраст
При этом нужно сделать так, чтобы возраст нельзя было
изменить извне напрямую, но при этом можно было бы получить
его значение
У каждого животного должен быть обязательный метод – ходить
А для птиц – летать. Эти методы должны выводить сообщения
(контекст придумайте)
Также должны быть общие для всех классов обязательные методы
– постареть - который увеличивает возраст на 1 и метод голос

При обращении к несуществующему методу или атрибуту животного
или птицы должен срабатывать пользовательский exception NotExistException
и выводить сообщение о том, что для текущего класса (имя класса),
такой метод или атрибут не существует – (имя метода или атрибута)

На основании этих классов создать отдельные классы для Свиньи,
Гуся, Курицы и Коровы – для каждого класса метод голос должен
выводить разное сообщение (хрю, гага, кудах, му)

После этого создать класс Ферма, в который можно передавать
список животных. Должна быть возможность получить каждое
животное, живущее на ферме как по индексу, так и через цикл for.
Также у фермы должен быть метод, который увеличивает возраст
всех животных, живущих на ней.
"""


class NotExistException(Exception):
    pass


class Animal(ABC):
    def __init__(self, age, name):
        self._age = age
        self.name = name

    def __getattr__(self, name):
        raise NotExistException(f'{self.__class__.__name__} '
                                f'hasn\'t attribute {name}')

    @abstractmethod
    def move(self):
        return 'move'

    @abstractmethod
    def get_older(self):
        self._age += 1

    @property
    def age(self):
        return self._age

    @abstractmethod
    def get_voice(self):
        return 'voiiiice'


class Bird(ABC):
    def __init__(self, age, name):
        self._age = age
        self.name = name

    def __getattr__(self, name):
        raise NotExistException(f'{self.__class__.__name__} '
                                f'hasn\'t attribute {name}')

    @abstractmethod
    def fly(self):
        return 'I believe - I can fly!'

    @abstractmethod
    def get_older(self):
        self._age += 1

    @property
    def age(self):
        return self._age

    @abstractmethod
    def get_voice(self):
        return 'flip flip flip'


class Pig(Animal):
    def __init__(self, age, name):
        super().__init__(age, name)

    def __str__(self):
        return f'!!I am a pig {self.name}!!'

    def get_older(self):
        self._age += 1

    def get_voice(self):
        return 'uiiiii uiiii'

    def move(self):
        return 'hurum hurum'


class Goose(Bird):
    def __init__(self, age, name):
        super().__init__(age, name)

    def __str__(self):
        return f'!!I am a goose {self.name}!!'

    def get_older(self):
        self._age += 1

    def get_voice(self):
        return 'gooouse uaa uaa'

    def fly(self):
        return 'I believe - I can fly!'


class Chicken(Bird):
    def __init__(self, age, name):
        super().__init__(age, name)

    def __str__(self):
        return f'!!I am a chicken {self.name}!!'

    def get_older(self):
        self._age += 1

    def get_voice(self):
        return 'ko ko ko'

    def fly(self):
        return 'I believe - I can fly!'


class Cow(Animal):
    def __init__(self, age, name):
        super().__init__(age, name)

    def __str__(self):
        return f'!!I am a cow {self.name}!!'

    def get_older(self):
        self._age += 1

    def get_voice(self):
        return 'moo'

    def move(self):
        return 'cok cok'


class Farm:
    def __init__(self, *animals):
        self.animals = animals

    def __getitem__(self, index):
        return self.animals[index]

    def increase_age_of_animals(self):
        for i in self.animals:
            i.get_older()


pig_1 = Pig(1, 'hur1')
goose_1 = Goose(3, 'ship-ship')
chicken_1 = Chicken(2, 'Patrik')
cow_1 = Cow(5, 'Burenka')

print(pig_1.move())
print(pig_1.get_voice())
print(goose_1.fly())

zoo = Farm(pig_1, goose_1, chicken_1, cow_1)

for animal in zoo:
    print(animal)


print(zoo)
print()
print(zoo[1])
print(zoo.increase_age_of_animals())

for animal in zoo:
    print(animal.age)

print(goose_1.color)
