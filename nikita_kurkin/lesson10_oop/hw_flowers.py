"""
Цветочница.

Определить иерархию и создать несколько цветов (Rose,
Tulips, violet, chamomile).
У каждого класса цветка следующие атрибуты:
Стоимость – атрибут класса, который определяется заранее
Свежесть (в днях), цвет, длинна стебля – атрибуты экземпляра
При попытке вывести информацию о цветке, должен отображаться цвет и тип
Rose = RoseClass(7, Red, 15)
Print(Rose) -> Red Rose (__str__)
Собрать букет (можно использовать аксессуары – отдельные классы со своей
стоимостью: например, упаковочная бумага) с определением ее стоимости.
У букета должна быть возможность определить время его увядания по среднему
времени жизни всех цветов в букете
Позволить сортировку цветов в букете на основе различных параметров
(свежесть/цвет/длина стебля/стоимость...)
Реализовать поиск цветов в букете по определенным параметрам.
Узнать, есть ли цветок в букете. (__contains__)
Добавить возможность получения цветка по индексу (__getitem__) либо
возможность пройтись по букету и получить
каждый цветок по-отдельности (__iter__)
"""


class RoseClass:
    price = 100

    def __init__(self, count_of_alive_day, color, length):
        """
        :param count_of_alive_day: count of day which flower will be fresh
        :param color: color of flower
        :param length: length of flower
        """
        self.count_of_alive_day = count_of_alive_day
        self.color = color
        self.length = length

    def __str__(self):
        return f'{self.color} {self.__class__.__name__}'


class TulipsClass(RoseClass):
    price = 80

    def __init__(self, count_of_alive_day, color, length):
        super().__init__(count_of_alive_day, color, length)

    def __str__(self):
        return f'{self.color} {self.__class__.__name__}'


class VioletClass(RoseClass):
    price = 70

    def __init__(self, count_of_alive_day, color, length):
        super().__init__(count_of_alive_day, color, length)

    def __str__(self):
        return f'{self.color} {self.__class__.__name__}'


class ChamomileClass(RoseClass):
    price = 120

    def __init__(self, count_of_alive_day, color, length):
        super().__init__(count_of_alive_day, color, length)

    def __str__(self):
        return f'{self.color} {self.__class__.__name__}'


class DecoratePaper:
    price = 45

    def __init__(self, count):
        self.count = count


class Bouquet:
    def __init__(self, lst):
        self.lst = lst

    def __str__(self):
        return f'Bouquet consist of {[i for i in self.lst]}'

    def __contains__(self, item):
        return item in self.lst

    def __getitem__(self, item):
        return self.lst[item]

    def __iter__(self):
        return iter(self.lst)

    def avg_time(self):
        sum_ = 0
        for k in self.lst:
            sum_ += k.count_of_alive_day
        avg = sum_ / len(self.lst)
        return avg

    def sorted_by(self, criteria):
        dict_filters = {
            'color': lambda flower: flower.color,
            'fresh': lambda flower: flower.count_of_alive_day,
            'length': lambda flower: flower.length,
            'price': lambda flower: flower.price

        }
        sort_ = list(sorted(self.lst, key=dict_filters[criteria]))
        return [flower for flower in sort_]

    def price(self):
        bouq_price = sum([i for i in self.lst.price])
        return bouq_price


# print(f'Bouquet with {rose1}, {tulip1}, {viol} and {chamom1}. '
#       f'Price of this is '
#       f'{rose1.price + tulip1.price + viol.price + chamom1.price} '
#       f'of rubbles. '
#       f'Average time of life is {bouq.avg_time()} days.')

if __name__ == '__main__':
    rose1 = RoseClass(10, 'Red', 30)
    tulip1 = TulipsClass(6, 'Yellow', 19)
    viol = VioletClass(11, 'Violet', 25)
    chamom1 = ChamomileClass(7, 'White', 17)
    flowers = [rose1, tulip1, viol, chamom1]
    bouq = Bouquet(flowers)

    print(bouq.price)
    print(bouq.sorted_by('fresh'))
    print(bouq)
