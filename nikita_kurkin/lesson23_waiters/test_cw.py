from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TestDemoQa:
    def test_check_radio_button(self, browser):
        browser.get('https://demoqa.com/radio-button')
        browser.find_element(By.XPATH, "//label[@for='yesRadio']").click()
        yes_ = browser.find_element(By.CSS_SELECTOR, ".mt-3").text
        assert yes_ == 'You have selected Yes'

    def test_smt(self, browser):
        browser.get('https://jsbin.com/cicenovile/1/edit?html,output')

        WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.NAME, "<proxy>")))
        r_frame = browser.find_element(By.NAME, "<proxy>")
        browser.switch_to.frame(r_frame)

        WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.NAME, "JS Bin Output "))
        )
        inter_frame = browser.find_element(By.NAME, "JS Bin Output ")
        browser.switch_to.frame(inter_frame)

        browser.find_element(By.TAG_NAME, 'a').click()

        browser.switch_to.alert.accept()

        browser.switch_to.default_content()

        login_button = browser.find_element(By.ID, 'loginbtn').text
        assert login_button == 'Login or Register'
