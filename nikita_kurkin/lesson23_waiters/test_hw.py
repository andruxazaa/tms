from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TestHomeWork:
    def test_dynamic_controls(self, browser):
        browser.get('http://the-internet.herokuapp.com/dynamic_controls')
        browser.find_element(By.XPATH, '//input[@type="checkbox"]').click()
        browser.find_element(
            By.XPATH, '//button[@onclick="swapCheckbox()"]'
        ).click()

        WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.ID, "message")))
        WebDriverWait(browser, 10).until(
            EC.invisibility_of_element(
                (
                    By.XPATH, '//input[@type="checkbox"]'
                )
            )
        )

        if not browser.find_element(
                By.CSS_SELECTOR, '#input-example input').is_enabled():
            browser.find_element(By.XPATH,
                                 '//button[@onclick="swapInput()"]').click()

        WebDriverWait(browser, 10).until(
            EC.presence_of_element_located(
                (By.XPATH, "//form[@id='input-example']/p")))

        assert browser.find_element(By.CSS_SELECTOR,
                                    '#input-example input').is_enabled()

    def test_frames(self, browser):
        browser.get('http://the-internet.herokuapp.com/frames')
        browser.find_element(By.LINK_TEXT, 'iFrame').click()
        WebDriverWait(browser, 10).until(
            EC.presence_of_element_located((By.ID, "mce_0_ifr"))
        )
        general_frame = browser.find_element(By.ID, "mce_0_ifr")
        browser.switch_to.frame(general_frame)

        text_ = browser.find_element(By.TAG_NAME, 'p').text
        assert text_ == 'Your content goes here.'
