import pytest
from func_2 import InputTypeError, reverse_to_float


class TestReverseDigits:
    @pytest.mark.parametrize("digit, expectedResult", [
        (123, 123.0),
        (1421, 1421.0),
        (0, 0.0)
    ])
    def test_reverse_to_floar(self, digit, expectedResult):
        assert reverse_to_float(digit) == expectedResult, "Something worng"

    @pytest.mark.xfail
    def test_wrong_revers_to_float(self):
        with pytest.raises(InputTypeError):
            reverse_to_float('a')
