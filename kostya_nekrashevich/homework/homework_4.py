""" 1. Перевести строку в массив "Robin Singh" => ["Robin”, “Singh"]
"I love arrays they are my favorite" => ["I", "love", "arrays", "they",
"are", "my", "favorite"] """
test_str = "Robin Singh"
array_str = test_str.split(" ")
print(array_str)
test_str_1 = "I love arrays they are my favorite"
array_str_2 = test_str_1.split(" ")
print(array_str_2)


""" 2. Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus.
Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”"""
test_list = ["Ivan", "Ivanou"]
city = "Minsk"
country = "Belarus"
text = f'Привет, {test_list[0]} {test_list[1]}! ' \
       f'Добро пожаловать' f' в {city} {country}'
print(text)


""" 3. Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
сделайте из него строку => "I love arrays they are my favorite". """
test_list_1 = ["I", "love", "arrays", "they", "are", "my", "favorite"]
print(' '.join(test_list_1))


""" 4. Создайте список из 10 элементов, вставьте на 3-ю позицию новое значение,
удалите элемент из списка под индексом 6. """
random_list = [i for i in range(10)]
random_list.insert(2, 11)
random_list.remove(random_list[5])
print(random_list)


""" 5. Есть 2 словаря
a = { 'a': 1, 'b': 2, 'c': 3}
b = { 'c': 3, 'd': 4,'e': “”}
1. Создайте словарь, который будет содержать в себе все элементы обоих
словарей
2. Обновите словарь “a” элементами из словаря “b”
3. Проверить что все значения в словаре “a” не пустые либо не равны нулю
4. Проверить что есть хотя бы одно пустое значение (результат выполнения
должен быть True)
5. Отсортировать словарь по алфавиту в обратном порядке
6. Изменить значение под одним из ключей и вывести все значения """
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ""}
merge = {**a, **b}
print(merge)
a.update(b)
print(a)
print(() in a.values())
print(0 in a.values())
print("" in a.values())
print(sorted(a, reverse=True))
a['b'] = 8
print(a)


""" 6.Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]
a. Вывести только уникальные значения и сохранить их в отдельную
переменную
b. Добавить в полученный объект значение 22
c. Сделать list_a неизменяемым
d. Измерить его длинну"""
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
list_b = [x for i, x in enumerate(list_a) if i == list_a.index(x)]
print(list_b)
list_b.append(22)
list_a = tuple(list_a)
print(len(list_a))


""" Задачи на закрепление форматирования:
1) Есть переменные a=10, b=25
Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
При решении задачи использовать оба способа форматирования """
a = 10
b = 25
print(f'Summ is {a + b} and diff = {a - b}.')
print('Summ is', a + b, 'and diff =', a - b, end='. \n')


""" 2) Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
Вывести “First child is <первое имя из списка>, second is “<второе>”,
and last one – “<третье>”” """
list_of_children = ['Sasha', 'Vasia', 'ikalai']
print(f'First child is {list_of_children[0]}, second is {list_of_children[1]},'
      f' and last one - {list_of_children[2]}')
