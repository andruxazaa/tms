"""
переводит переданное число в тип float, если передана строка вызывает
пользовательскую ошибку InputTypeError (создать ее самому)
"""


class InputTypeError(Exception):
    def __init__(self, message="int type should be used"):
        super().__init__(message)


def convert_to_float(arg):
    if isinstance(arg, int):
        return float(arg)
    raise InputTypeError
