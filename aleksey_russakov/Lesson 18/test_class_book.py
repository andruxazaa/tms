from class_book import Book, PageNumberError, YearError, AuthorNameError,\
    PriceError
from unittest import TestCase, expectedFailure
from unittest.mock import patch


def mock_price(price):
    return price


# Замокал функцию validate_price с помощью mock_price
class TestPrice(TestCase):
    @patch('test_class_book.Book.validate_price', side_effect=mock_price)
    def test_price(self, arg):
        self.assertEqual(Book.validate_price(99), 99)


class TestBookPage(TestCase):

    def test_page_num_validation_ok(self):
        self.assertEqual(Book.validate_pages(4000), 4000)

    @expectedFailure
    def test_page_num_validation_fail(self):
        self.assertEqual(Book.validate_pages(4001), 4001)

    def test_page_error(self):
        with self.assertRaises(PageNumberError):
            Book.validate_pages(4001)


class TestBookYear(TestCase):

    def test_year_validation_ok(self):
        self.assertEqual(Book.validate_year(1980), 1980)

    @expectedFailure
    def test_year_validation_fail(self):
        self.assertEqual(Book.validate_year(1979), 1979)

    def test_year_error(self):
        with self.assertRaises(YearError):
            Book.validate_year(1979)


class TestBookAuthor(TestCase):

    def test_author_validation_ok(self):
        self.assertEqual(Book.validate_author_name("Abc"), "Abc")

    @expectedFailure
    def test_author_validation_fail(self):
        self.assertEqual(Book.validate_author_name("123"), "123")

    def test_author_error(self):
        with self.assertRaises(AuthorNameError):
            Book.validate_author_name("123")


class TestBookPrice(TestCase):

    def test_price_validation_ok(self):
        self.assertEqual(Book.validate_price(100), 100)

    @expectedFailure
    def test_price_validation_fail(self):
        self.assertEqual(Book.validate_price(99), 99)

    def test_price_error(self):
        with self.assertRaises(PriceError):
            Book.validate_price(99)
