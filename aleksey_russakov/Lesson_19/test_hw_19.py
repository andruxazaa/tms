import pytest
from aleksey_russakov.Lesson_19.check_str_lower import check_lower
from aleksey_russakov.Lesson_19.check_spec_symbol import check_spec_sym
from aleksey_russakov.Lesson_19.conv_to_float import to_float, InputTypeError
import time

"""
    2.Для каждой функции создать параметризированный тест. В качестве
параметров  тест принимает значение, которое должна обработать функция и
ожидаемый результат ее выполнения.
    3.Каждый тест должен быть вызван по 3 раза

    Это делается декоратором @pytest.mark.repeat(3), но т.к. в каждом
тесте 3 параметра, я посчитал, что этого делать не надо.
"""


@pytest.mark.parametrize("string,expected_result",
                         [("Abc", False), ("abc", True), ("1", False)])
def test_check_lower(string, expected_result):
    assert check_lower(string) == expected_result, "Unexpected result"


@pytest.mark.parametrize("number,expected_result",
                         [(2, 2.0), (-21, -21.0), (0, 0.0)])
def test_to_float(number, expected_result):
    assert to_float(number) == expected_result, "Unexpected result"


@pytest.mark.parametrize("symbol,expected_result",
                         [("/", True), ("a", False), ("1", False)])
def test_check_spec_sym(symbol, expected_result):
    assert check_spec_sym(symbol) == expected_result, "Unexpected result"


"""
    4.Для 2-ой функции написать отдельный тест, который будет ожидать ошибку
InputTypeError, и если эта ошибка не произошла, падать!
"""


def test_to_float_exception():
    with pytest.raises(InputTypeError):
        assert to_float("1")


"""
    5.Написать фикстуру, которая будет выводить время начала запуска всех
тестов и когда все тесты отработали. По итогу она должна отработать один раз!
"""


@pytest.fixture(autouse=True, scope='session')
def footer_function_scope():
    start = time.time()
    yield
    stop = time.time()
    print(f"\nStart time tests: {start}, Finish time tests: {stop}")


"""
    6.Написать фикстуру, которая будет выводить имя файла, из которого запущен
тест. Если тесты находятся в одном файле, для них фикстура должна отработать
один раз!
"""


@pytest.fixture(autouse=True, scope="module")
def file_name(request):
    print(f"{request.node.name} - имя файла, из которого запущен тест")


"""
    7.Написать фикстуру, которая для каждого теста будет выводить следующую
информацию:
   А) Информацию о фикстурах, которые используются в этом тесте
   Б) Информацию о том, сколько времени прошло между запуском сессии и
запуском текущего теста
"""


@pytest.fixture(autouse=True, params=[time.time()])
def check_time_and_fixtures(request):
    print(f"\nВ тесте {request.node.name} используются фикстуры:"
          f" {request.fixturenames}")
    print(f"Прошло {time.time() - request.param} между запуском сессии и "
          f"запуском текущего теста")
