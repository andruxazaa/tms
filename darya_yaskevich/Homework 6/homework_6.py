# 1. Программа принимает число, номер кредитной карты
# (число может быть четным или не четным), и проверяет может ли такая
# карта существовать. Предусмотреть защиту от ввода букв, пустой строки и т.д.
def is_valid_card(card_number: str):
    """
    This function verifies if the credit card number is valid using
    Luhn algorithm.
    """
    # make an array of reversed digits
    numbers_reversed = [int(n) for n in card_number[::-1]]

    # double numbers in odd indexes, if doubled num > 9 - subtract 9
    final_numbers = []
    count = 1
    for num in numbers_reversed:
        if count % 2 == 0:
            doubled_num = num * 2
            if doubled_num > 9:
                doubled_num -= 9
            final_numbers.append(doubled_num)
        else:
            final_numbers.append(num)
        count += 1

    # sum all numbers and check % 10
    return sum(final_numbers) % 10 == 0


credit_card = input('Please enter a card number: ').strip()

if not credit_card.isdigit():
    print('This card number is not valid.')
else:
    if is_valid_card(credit_card):
        print('This is a valid card number.')
    else:
        print('This card number is not valid.')


# 2. Подсчет количества букв. На вход подается строка, например, "cccbba";
# результат работы программы - строка “c3b2a".
def count_symbols(a: str):
    """
    This functions counts the number of identical letters in a string
    and returns edited string with a counter instead of repeated letters.
    :param a: "aaabbdefffff"
    :return: "a3b2def5"
    """
    a_edited = [a[0]]
    counter = 1
    for i in range(1, len(a)):
        if a[i] == a[i - 1]:
            counter += 1
            if i == len(a) - 1:
                a_edited.append(str(counter))
            elif a[i] != a[i + 1]:
                a_edited.append(str(counter))
                counter = 1
        else:
            a_edited.append(a[i])
    a_edited = ''.join(a_edited)
    return a_edited


print(count_symbols(input('Enter the string: ')))


# 3. Простейший калькулятор v0.1
# Реализуйте программу, которая спрашивала у пользователя,
# какую операцию он хочет произвести над числами, а затем запрашивает
# два числа и выводит результат. Проверка деления на 0.
def simple_calculator(a: int, b: int, operator: str):
    """
    This function performs one of the following operations on given numbers:
    addition, subtraction, multiplication or division, depending on
    the 3d parameter. It returns the result of an operation (int).
    """
    if operator == '+':
        return f'Sum: {a + b}'
    elif operator == '-':
        return f'Difference: {a - b}'
    elif operator == '/':
        quotient = a // b
        reminder = a % b
        return f'Quotient: {quotient}, reminder: {reminder}'
    elif operator == '*':
        return f'Product: {a + b}'


while True:
    choice = input('Select an operation ("+", "-", "/" or "*"): ')
    num1 = input('Enter the first number: ')
    num2 = input('Enter the second number: ')
    if not num1.isdigit() or not num2.isdigit():
        print('Please enter a number.')
        continue
    elif choice not in '+-/*':
        print('Please enter "+", "-", "/" or "*".')
        continue
    elif num2 == '0' and choice == '/':
        print('Division by zero!')
        continue
    else:
        break


num1 = int(num1)
num2 = int(num2)

print(simple_calculator(num1, num2, choice))


# 4. Написать функцию с изменяемым числом входных параметров.
# При объявлении функции предусмотреть один позиционный и один именованный
# аргумент, который по умолчанию равен None (в примере это аргумент 'name').
# Также предусмотреть возможность передачи нескольких именованных и
# позиционных аргументов. Функция должна возвращать следующее:
# result = function(1, 2, 3, name=’test’, surname=’test2’, some=’something’)
# print(result)
# 🡪 {“mandatory_position_argument”: 1,
# “additional_position_arguments”: (2, 3),
# “mandatory_named_argument”: {“name”: “test2”},
# “additional_named_arguments”: {“surname”: “test2”, “some”: “something”}}
def function(a: int, *args, name=None, **kwargs):
    """
    This is the function with a variable number of parameters.
    :param a: value of the 'mandatory_position_argument' key
    :param args: tuple, value of the 'additional_position_arguments' key
    :param name: dict, value of the 'mandatory_named_argument' key
    :param kwargs: dict, value of the 'additional_named_arguments' key
    :return: dictionary
    """
    d = {'mandatory_position_argument': a,
         'mandatory_named_argument': {'name': name}}

    if args:
        d['additional_position_arguments'] = args
    if kwargs:
        d['additional_named_arguments'] = kwargs
    return d


result = function(1, 2, 3, name='test', surname='test2', some='something')
print(result)


# 5. Работа с областями видимости.
# На уровне модуля создать список из 3-х элементов.
# Написать функцию, которая принимает на вход этот список и добавляет
# в него элементы. Функция должна вернуть измененный список.
# При этом исходный список не должен измениться.
def change_list(lst: list):
    """
    This function adds element 'symbol' to the list and returns modified list.
    """
    changed_lst = lst[:]
    changed_lst.append('symbol')
    return changed_lst


my_list = [1, 2, 3]
print(my_list)
print(change_list(my_list))


# 6. Написать функцию, принимающую на вход список из чисел, строк и таплов.
# Функция должна вернуть сколько в списке элементов приведенных данных
# print(my_function([1, 2, 'a', (1, 2), 'b'])
# 🡪 {'int': 2, 'str': 2, 'tuple': 1}
def type_counter(lst: list):
    """
    This function counts how many strings, integers and tuples are
    in the list.
    :param lst: a list of integers, strings and tuples
    :return: {'int': 2, 'str': 2, 'tuple': 1}
    """
    types_counted = dict.fromkeys(['int', 'str', 'tuple'], 0)
    for elem in lst:
        if isinstance(elem, int):
            types_counted['int'] += 1
        elif isinstance(elem, str):
            types_counted['str'] += 1
        elif isinstance(elem, tuple):
            types_counted['tuple'] += 1
    return types_counted


my_list = [1, 2, 'a', (1, 2), 'b']
print(type_counter(my_list))

# 7. Написать пример, где hash от объекта 1 и 2 одинаковые, а id разные.
number1 = 3
number2 = 3.0
print(hash(number1) == hash(number2))
print(id(number1) == id(number2))


# 8. Написать функцию, которая проверяет есть ли в списке объекты,
# которые можно вызвать.
def is_callable(lst: list):
    callable_elements = []
    for elem in lst:
        if callable(elem):
            callable_elements.append(elem)
    if callable_elements:
        return f'{True}, {callable_elements}'
    else:
        return False


print(is_callable([is_callable]))
