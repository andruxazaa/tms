def special_chars(arg):
    """
    Checks whether an arg parameter is a special character.
    :param arg: argument to check
    :return: True or False
    """
    return arg in '[@_!#$%^&*()<>?/\\|}{~:;\"\'.,]'
