from datetime import datetime


# 1. Напишите функцию, которая возвращает строку: “Hello world!”
def say_hello():
    return 'Hello world!'


# 2. Напишите функцию, которая вычисляет сумму трех чисел
# и возвращает результат в основную ветку программы.
def find_sum(a, b, c):
    return a + b + c


# 3. Придумайте программу, в которой из одной функции вызывается вторая.
# При этом ни одна из них ничего не возвращает в основную ветку программы,
# обе должны выводить результаты своей работы с помощью функции print().
def lucky(num=7):
    print(f'Lucky number is {num}')


def sentence():
    lucky()


# 4. Напишите функцию, которая не принимает отрицательные числа
# и возвращает число наоборот.
# 123456789 => 987654321
def positive_reversed(num):
    if num < 0:
        print("Negative numbers are not accepted.")
    else:
        reversed_num = int(str(num)[::-1])
        return int(reversed_num)


# 5. Напишите функцию fib(n), которая по данному целому неотрицательному n
# возвращает n-e число Фибоначчи.
def fib(n):
    num1 = 1
    num2 = 1
    counter = 0
    while counter <= n - 1:
        num1, num2 = num2, num1 + num2
        counter += 1
        if counter == n - 1:
            return num1


# 6. Напишите функцию, которая проверяет на то,
# является ли строка палиндромом или нет.
def is_palindrome(s: str):
    s_low = s.lower()
    return s_low == s_low[::-1]


# 7. У вас интернет магазин, надо написать функцию которая проверяет
# что введен правильный купон и он еще действителен.
# check_сoupon("123", "123", "July 9, 2015", "July 9, 2015")  == True
# check_сoupon("123", "123", "July 9, 2015", "July 2, 2015")  == False


def check_coupon(entered_code: str, correct_code: str,
                 current_date_str: str, expiration_date_str: str):
    current_date = datetime.strptime(current_date_str, '%B %d, %Y')
    expiration_date = datetime.strptime(expiration_date_str, '%B %d, %Y')

    return entered_code == correct_code and current_date <= expiration_date


# 8. Фильтр.
# Функция принимает на вход список, проверяет есть ли эти элементы в
# списке exclude, если есть удаляет их и возвращает список
# с оставшимися элементами.
# ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"] => []
def filter_list(lst: list):
    exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"]
    remaining = []
    for elem in lst:
        if elem not in exclude:
            remaining.append(elem)
    return remaining
