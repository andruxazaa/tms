# Генераторы:
# Напишите функцию-генератор, которая вычисляет числа фибоначчи:

def gen_func(n: int):
    """
    Yield the Fib_number
    """
    a = 0
    print(a)
    b = 1
    print(b)
    c = a + b
    for i in range(n - 2):
        yield c
        a = b
        b = c
        c = a + b


for i in gen_func(7):
    print(i)

# Напишите генератор списка который принимает список numbers = [34.6, -203.4,
# 44.9, 68.3, -12.2, 44.6, 12.7] и возвращает новый список только
# с положительными числами

numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]
numbers_pos = [i for i in numbers if i > 0]
print(numbers_pos)

# Необходимо составить список чисел которые указывают на длину слов в строке:
# sentence = "the quick brown fox jumps over the lazy dog",
# но только если слово не "the".
sentence = "the quick brown fox jumps over the lazy dog"
sentence = sentence.split()
sent_list = [len(i) for i in sentence if i != 'the']
print(sent_list)

# 1. Петя перешел в другую школу. На уроке физкультуры ему понадобилось
# определить своё место в строю. Помогите ему это сделать.
# Программа получает на вход не возрастающую последовательность
# натуральных чисел, означающих рост каждого человека в строю.
# После этого вводится число X – рост Пети.
# Выведите номер, под которым Петя должен встать в строй.

height_list = [187, 182, 176, 172, 170]
height_petya = int(input('Petyas height is: '))
for i in range(len(height_list)):
    if height_list[i] < height_petya:
        height_list.insert(height_list.index(height_list[i]), height_petya)
        break
print(len(height_list) - height_list[::-1].index(height_petya))

# 2. Дан список чисел. Выведите все элементы списка, которые больше
# предыдущего элемента.
# [1, 5, 2, 4, 3]  #=> [5, 4]
# [1, 2, 3, 4, 5] #=> [2, 3, 4, 5]

inp_list = [1, 5, 2, 4, 3]
out_list = [inp_list[i] for i in range(len(inp_list)) if
            inp_list[i] > inp_list[i - 1]]
print(out_list)


# 3. Напишите программу, принимающую зубчатый массив любого типа и
# возвращающего его "плоскую" копию.

def rec_func(list_a: list):
    """
    This function makes an unstapped copy of a list
    """
    for i in list_a:
        if isinstance(i, list):
            rec_func(i)
        else:
            list_b.append(i)
    return list_b


list_a = [1, 2, [3, 4, [5, 6], 7], [8, [9, [10]]]]
list_b = []
print(rec_func(list_a))


# 4. Напишите функцию, которая принимает на вход одномерный массив и два
# числа - размеры выходной матрицы. На выход программа должна подавать матрицу
# нужного размера, сконструированную из элементов массива.
# reshape([1, 2, 3, 4, 5, 6], 2, 3) =>
# [
# [1, 2, 3],
# [4, 5, 6]
# ]
# reshape([1, 2, 3, 4, 5, 6, 7, 8,], 4, 2) =>
# [
# [1, 2],
# [3, 4],
# [5, 6],
# [7, 8]
# ]

def list_converter(list_a: list, _y: int, _x: int):
    """
    this function converts the input list to the list with  input parameters
    """
    list_b = [[] * _x for i in range(_y)]
    for i in list_b:
        for j in range(_x):
            i.append(list_a.pop(0))
    return list_b


list_a = [1, 2, 3, 4, 5, 6, 7, 8]
_y = 4
_x = 2

for i in list_converter(list_a, _y, _x):
    print(i)

# another circle FOR logics:
# k=0
# for i in list_b:
# for j in range(_x):
# i.append(list_a[k])
# k+=1
