from gitlab_helper import GitLabHelper, SecondGitLabHelper

AUTH_TOKEN = "glpat-vwKRuPp-CYzhFhkmcKVv"
PROJECT_ID = "31211175"


def test_create_new_branch():
    api_client = GitLabHelper(AUTH_TOKEN)
    branches = api_client.get_branches(PROJECT_ID)
    new_branch_name = "test_ne_new_branch"

    # create branch
    new_branch = api_client.create_branch(PROJECT_ID, new_branch_name,
                                          branches[0]["name"])
    all_branches = api_client.get_branches(PROJECT_ID)

    # filter branches by a new one
    f_ = [branch for branch in all_branches if
          new_branch["name"] == branch["name"]]
    assert f_, "Created branch is not found"


def test_second_create_new_branch():
    api_client = SecondGitLabHelper(AUTH_TOKEN)
    branches = api_client.get_branches(PROJECT_ID)
    new_branch_name = "test_ne_new_branch"

    # create branch
    new_branch = api_client.create_branch(PROJECT_ID, new_branch_name,
                                          branches[0]["name"])
    all_branches = api_client.get_branches(PROJECT_ID)

    # filter branches by a new one
    f_ = [branch for branch in all_branches if
          new_branch["name"] == branch["name"]]
    assert f_, "Created branch is not found"


def test_comment_creation():
    api_client = SecondGitLabHelper(AUTH_TOKEN)
    # check all commits
    commits = api_client.get_commits(PROJECT_ID)
    print(commits)
    # choose a single one
    sha = '015cd87849296be0302c1ed18db1068c72d6380c'
    # check this single one commit
    single_commit = api_client.get_a_single_commit(PROJECT_ID, sha)
    print(single_commit)
    # check its comments
    single_commit_comments = api_client.get_comments_of_the_commit(PROJECT_ID,
                                                                   sha)
    print(f'\n{single_commit_comments}')
    comments_count = len(single_commit_comments)
    note = 'Sorry, i have to write something here..'
    new_comment = api_client.create_a_comment_to_the_commit(PROJECT_ID, sha,
                                                            note)
    single_commit_comments_after = api_client.get_comments_of_the_commit(
        PROJECT_ID, sha)
    print(single_commit_comments_after)
    comments_count_after = len(single_commit_comments_after)
    diff_count = comments_count_after - comments_count
    f_ = [comment for comment in single_commit_comments_after if
          new_comment["note"] == comment["note"]]
    assert diff_count == 1 and f_, "Created comment is not found"
