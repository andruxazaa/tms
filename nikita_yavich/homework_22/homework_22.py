import pytest
from selenium.webdriver.common.by import By
import logging

logging.basicConfig(filename='homework_22.log',
                    level=logging.INFO, force=True,
                    format='%(asctime)s -%(levelname)s : %(message)s')

LOGO_BAR = "//div[@class= 'app_logo']"
CART = "//a[@class='shopping_cart_link']"
BURGER_MENU = "//button[@id='react-burger-menu-btn']"
BURGER_ALL_ITEMS_CHOICE = "//a[@id='inventory_sidebar_link']"
BURGER_ABOUT_CHOICE = "//a[@id='about_sidebar_link']"
BURGER_LOGOUT_CHOICE = "//a[@id='logout_sidebar_link']"
BURGER_RESET_APP_STATE_CHOICE = "//a[@id='reset_sidebar_link']"
BURGER_CLOSE = "//button[@id='react-burger-cross-btn']"
PRODUCTS_TEXT = "//span[@class='title']"
UNIT_LOGO = "//div[@class='peek']"
FOOTER_INFO = "//div[@class='footer_copy']"
FOOTER_UNIT = "//img[@alt='Swag Bot Footer']"
TWITTER_LINK = "//a[@href='https://twitter.com/saucelabs']"
FACEBOOK_LINK = "//a[@href='https://www.facebook.com/saucelabs']"
LINKEDIN_LINK = "//a[@href='https://www.linkedin.com/company/sauce-labs/']"
SORT_MENU = "//select[@class='product_sort_container']"
SORT_MENU_AZ = "//option[@value='az']"
SORT_MENU_ZA = "//option[@value='za']"
SORT_MENU_LOHI = "//option[@value='lohi']"
SORT_MENU_HILO = "//option[@value='hilo']"


def test_goods_info(driver, login, goods_selector):
    good_name = driver.find_element(
        By.CSS_SELECTOR,
        f".inventory_list>.inventory_item:nth-child({goods_selector}) "
        f".inventory_item_name")
    good_price = driver.find_element(
        By.CSS_SELECTOR,
        f".inventory_list>.inventory_item:"
        f"nth-child({goods_selector}) .inventory_item_price")
    print(f'\n{good_name.text} costs {good_price.text}$')
    logging.info(f'\n{good_name.text} costs {good_price.text}$')


def test_goods_description(driver, login, goods_selector):
    good_description = driver.find_element(
        By.CSS_SELECTOR,
        f".inventory_list>.inventory_item:"
        f"nth-child({goods_selector}) .inventory_item_desc")

    print(f'\n Description of the good #{goods_selector}: \n'
          f' {good_description.text}')
    logging.info(f'\n Description of the good #{goods_selector}: \n'
                 f' {good_description.text}')


def test_goods_add_to_cart_buttons(driver, login, goods_selector):
    driver.find_element(
        By.CSS_SELECTOR,
        f".inventory_list>.inventory_item:nth-child({goods_selector}) .btn")

    print(f'\n "Add to cart" button #{goods_selector} presents.')
    logging.info(f'\n "Add to cart" button #{goods_selector} presents.')


@pytest.mark.parametrize('element_xpath',
                         [LOGO_BAR, CART, BURGER_MENU,
                          BURGER_ALL_ITEMS_CHOICE,
                          BURGER_RESET_APP_STATE_CHOICE,
                          BURGER_LOGOUT_CHOICE, BURGER_ABOUT_CHOICE,
                          BURGER_CLOSE,
                          PRODUCTS_TEXT,
                          UNIT_LOGO, FOOTER_INFO, FOOTER_UNIT, TWITTER_LINK,
                          FACEBOOK_LINK, LINKEDIN_LINK, SORT_MENU,
                          SORT_MENU_AZ, SORT_MENU_ZA, SORT_MENU_HILO,
                          SORT_MENU_LOHI])
def test_other_elements(driver, login, element_xpath):
    driver.find_element(By.XPATH, element_xpath)
    print(f'\n Element with XPATH: {element_xpath} presents.')
    logging.info(f'\n Element with XPATH: {element_xpath} presents.')
