from andrey_viatoshkin.home_work_24.pages.base_page import BasePage
from andrey_viatoshkin.home_work_24.pages.books_page_locators import\
    BooksPagesLocators
from andrey_viatoshkin.home_work_24.pages.book_detail_page import\
    BookDetailPage


class BooksPage(BasePage):
    URL = 'http://selenium1py.pythonanywhere.com/en-gb/catalogue' \
          '/category/books_2/'

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def find_book_by_name(self, title):
        list_of_books = self.driver.find_elements(
            *BooksPagesLocators.BOOK_CARDS)
        for book in list_of_books:
            if book.text == title:
                book.click()
                return BookDetailPage(self.driver, self.url)
