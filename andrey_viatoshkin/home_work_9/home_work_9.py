# 1 Создайте класс book с именем книги, автором, кол-м страниц, ISBN, флагом
# , зарезервирована ли книги или нет. Создайте класс пользователь который
# может брать книгу, возвращать, бронировать. Если другой пользователь хочет
# взять зарезервированную книгу(или которую уже кто-то читает - надо ему
# про это сказать).

class Book:
    reserved_name = ''

    def __init__(self, name, author, pages, isbn, is_reserved=False):
        self.name = name
        self.author = author
        self.pages = pages
        self.isbn = isbn
        self.is_reserved = is_reserved


class User:

    def __init__(self, name):
        self.name = name

    def get_book(self, book):
        if book.is_reserved:
            print('Book is already taken')
        else:
            book.is_reserved = True
            book.reserved_name = self.name
            print('Book has been taken')
        return book.reserved_name

    def return_book(self, book):
        if self.name == book.reserved_name and book.is_reserved:
            book.is_reserved = False
            print('Book has been returned')
        else:
            print('This is not your book')
        return book.is_reserved

    def book_book(self, book):
        if book.is_reserved:
            print('Book is already booked')
        else:
            book.is_reserved = True
            book.reserved_name = self.name
            print('Book has been booked')
        return book.reserved_name


book_1 = Book('Mist', 'King', 100, 1001)
book_2 = Book('The Dark Tower', 'King', 100, 1002)
user_1 = User('Andrey')
user_2 = User('Ivan')
user_3 = User('Ivan1')

user_1.return_book(book_1)
user_1.get_book(book_1)
user_1.get_book(book_1)
user_2.book_book(book_2)
user_1.return_book(book_2)
user_1.return_book(book_1)
user_1.return_book(book_1)
# user_2.get_book(book_2)


# 2 Банковский вклад
# Создайте класс инвестиция. Который содержит необходимые поля и методы,
# например сумма инвестиция и его срок.Пользователь делает инвестиция в
# размере N рублей сроком на R лет под 10% годовых (инвестиция с
# возможностью ежемесячной капитализации - это означает, что проценты
# прибавляются к сумме инвестиции ежемесячно). Написать класс Bank, метод
# deposit принимает аргументы N и R, и возвращает сумму, которая
# будет на счету пользователя

class Investment:
    def __init__(self, summ, term, percent):
        self.sum = summ
        self.term = term
        self.percent = percent


class Depositor:
    def __init__(self, name):
        self.name = name


class Bank(Investment, Depositor):
    def __init__(self, summ, term, name):
        self.summ = summ
        self.term = term
        self.name = name

    def deposit(self, summ, term):
        '''
        Allows to count deposit based on 10%
        '''
        for i in range(term * 12):
            summ = summ + (summ * 0.1)
        return summ


bank_1 = Bank(1000, 10, 10, 'Andrey')
print(bank_1.deposit(1000, 10))
