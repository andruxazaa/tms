import xml.etree.ElementTree as ET
import json
import yaml

# 1. работа с xml файлом
# Разработайте поиск книги в библиотеке по ее автору(часть имени)/цене/
# заголовку/описанию.


content = ET.parse('library.xml').getroot()


def find_book(input_value):
    for child in content:
        if child.find('author').text in input_value or\
                child.find('title').text == input_value or\
                child.find('genre').text == input_value or\
                child.find('price').text == input_value or\
                child.find('description').text == input_value:
            print('title', child.find('title').text,
                  '\ngenre', child.find('genre').text,
                  '\nprice', child.find('price').text,
                  '\ndescription', child.find('description').text)


# 2. работа с json файлом
# Разработайте поиск учащихся в одном классе, посещающих одну секцию.
# Фильтрацию учащихся по их полу. Поиск ученика по имени(часть имени)

def search_by_section_and_class(class_, section):
    with open('students.json') as json_data:
        data = json.load(json_data)
    for row in data:
        if section in row['Club'] and class_ in row['Class']:
            print(row['Name'])


def filter_by_gender(gender):
    with open('students.json') as json_data:
        data = json.load(json_data)
    for row in data:
        if gender in row['Gender']:
            print(row['Name'])


def search_by_name(name):
    with open('students.json') as json_data:
        data = json.load(json_data)
    for row in data:
        if name in row['Name']:
            print(row['Name'])

# 3. работа с yaml файлом
# * Напечатайте номер заказа
# * Напечатайте адрес отправки
# * Напечатайте описание посылки, ее стоимость и кол-во
# * Сконвертируйте yaml файл в json
# * Создайте свой yaml файл


with open('order.yaml') as yaml_file:
    parsed_yaml_file = yaml.load(yaml_file, Loader=yaml.FullLoader)

print('invoice', parsed_yaml_file['invoice'])
print(''.join(str(e) for e in (list(parsed_yaml_file['bill-to']['address']
                                    .values()))))
package = parsed_yaml_file['product']
for item in package:
    print(item['quantity'], item['description'], item['price'])

str_object = json.dumps(parsed_yaml_file, sort_keys=True, default=str)
json_obj = json.loads(str_object)

with open('from_yaml_to_json.json', 'w') as f:
    json.dump(json_obj, f)

with open('toyaml.yaml', 'w') as f:
    yaml.dump(parsed_yaml_file, f)
