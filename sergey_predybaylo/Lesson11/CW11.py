from abc import ABC, abstractmethod


class Animal(ABC):
    @abstractmethod
    def move(self):
        print('I can move')

    @abstractmethod
    def sleep(self):
        print('I can sleep')

    @abstractmethod
    def eat(self):
        print('I can eat')

    @abstractmethod
    def drink(self):
        print('I can drink')


class Pet(Animal):
    @abstractmethod
    def love(self):
        print('I can love')

    @abstractmethod
    def eat_slip(self):
        print('I can eat slippers')


class Wildpet(Animal):
    @abstractmethod
    def hunt(self):
        print('I can hunt')

    @abstractmethod
    def dig_hole(self):
        print('I can dig a hole')


class Cat(Pet):
    def love(self):
        print('I can love')

    def eat_slip(self):
        print('I can eat slippers')

    def move(self):
        print('I can move')

    def eat(self):
        print('I can eat')

    def drink(self):
        print('I can drink')

    def sleep(self):
        print('I can sleep')


class Lion(Wildpet):
    def hunt(self):
        print('I can hunt')

    def dig_hole(self):
        print('I can dig a hole')

    def move(self):
        print('I can move')

    def eat(self):
        print('I can eat')

    def drink(self):
        print('I can drink')

    def sleep(self):
        print('I can sleep')


cat1 = Cat()
cat1.love()
cat1.eat_slip()
cat1.move()

lion1 = Lion()
lion1.hunt()
lion1.dig_hole()


class Book:
    def __init__(self, pages, autor, year, price):
        self.pages = self.validate_page(pages)
        self.autor = self.validate_author(autor)
        self.year = self.validate_year(year)
        self.price = self.validate_price(price)

    @staticmethod
    def validate_page(page):
        if page < 4000:
            return page
        raise MyException_page

    @staticmethod
    def validate_author(author):
        if author.isalpha:
            return author
        raise MyException_author

    @staticmethod
    def validate_price(price):
        if price > 100 and price < 5000:
            return price
        raise MyException_price

    @staticmethod
    def validate_year(years):
        if years > 1950:
            return years
        raise MyException_year


class MyException_page(Exception):
    def __init__(self, message='Маленькое количество страниц!!'):
        super().__init__(message)


class MyException_year(Exception):
    def __init__(self, message='Слишком старый год!!'):
        super().__init__(message)


class MyException_price(Exception):
    def __init__(self, message='Слишком дорого!'):
        super().__init__(message)


class MyException_author(Exception):
    def __init__(self, message='Другие символы в авторе!'):
        super().__init__(message)


book = Book(250, 'name', 3000, 500)

"""
Создать lambda функцию, которая принимает
на вход имя и выводит его в формате
“Hello, {name}”
"""
# lambd_func = lambda x, y: x + y
# a = 'Hello '
# b = str(input('Введите имя'))
# print(lambd_func(a, b))

"""2 способ"""
# func = lambda name: f'Hello {name}'
# print(func('Karina'))

"""
Создать lambda функцию, которая принимает
на вход список имен и выводит их
в формате “Hello, {name}” в другой список
"""

# func1 = lambda name: [f'Hello {elem}' for elem in name]

# print(func1(['Karina', 'Sergey']))

# func1 = map(lambda name: f'Hello {name}',['Karina', 'Sergey'])

# print(list(func1))

"""
Дан список чисел. Вернуть список, где каждый число переведено в строку
[5, 3] -> [‘5’, ‘3’]
"""

func3 = map(lambda x: str(x), [5, 3])
print(list(func3))

"""
Дан список имен, отфильтровать все имена, где есть буква k
"""
func4 = list(filter(lambda x: 'k' in x,
                    ['karina', 'Sergei', 'kati']))

print(func4)

p = {"a": 10, "b": 20, "c": 0, "d": -1}
print(sorted(p, key=lambda p: p))
print({k: v for k, v in sorted(p.items(), key=lambda item: item[1])})
