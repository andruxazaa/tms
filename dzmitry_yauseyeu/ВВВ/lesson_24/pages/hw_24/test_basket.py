from basket_page import BasketPage
from basket_locators import BasketPageLocators


def test_open_basket_page(driver):
    page = BasketPage(driver)
    page.open_page()
    page.empty_basket()

    assert driver.find_element(*BasketPageLocators.EMPTY_INDICATOR)
